document.getElementById('calcular').addEventListener('click', function(){
    const cantidad = parseFloat(document.getElementById('cantidad').value);

    if(isNaN(cantidad)){
        alert("Por favor, ingresa una cantidad válida.");
        return;
    }

    const celsiusToFahrenheit = document.getElementById('celsiusToFahrenheit').checked;

    if(celsiusToFahrenheit){
        const resultado = (cantidad * 9/5) + 35;
        document.getElementById('resultado').textContent = resultado.toFixed(2) + " °F";
    } else{
        const resultado = (cantidad - 32) * 5/9;
        document.getElementById('resultado').textContent = resultado.toFixed(2) + " °C";
    }
});

document.getElementById("limpiar").addEventListener("click", function () {
    document.getElementById("cantidad").value = "0";
    document.getElementById("celsiusToFahrenheit").checked = true;
    document.getElementById("resultado").textContent = "0";
});