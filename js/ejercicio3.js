// Función para generar una edad aleatoria entre un rango específico
function generarEdad(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Función para calcular el conteo de edades por grupo
function calcularConteoEdades(edades) {
    let bebes = 0;
    let ninos = 0;
    let adolescentes = 0;
    let adultos = 0;
    let ancianos = 0;
    let sumaEdades = 0;

    for (const edad of edades) {
        if (edad < 2) {
            bebes++;
        } else if (edad >= 2 && edad < 13) {
            ninos++;
        } else if (edad >= 13 && edad < 20) {
            adolescentes++;
        } else if (edad >= 20 && edad < 65) {
            adultos++;
        } else {
            ancianos++;
        }
        sumaEdades += edad;
    }

    // Calcular la edad promedio
    const edadPromedio = sumaEdades / edades.length;

    // Mostrar los resultados en las etiquetas
    document.getElementById("edades").textContent = "Edades: " + edades.join(', ');
    document.getElementById("conteo").textContent = "Conteo de Edades: Bebés: " + bebes + ", Niños: " + ninos + ", Adolescentes: " + adolescentes + ", Adultos: " + adultos + ", Ancianos: " + ancianos;
    document.getElementById("totalBebes").textContent = "Total Bebés: " + bebes;
    document.getElementById("totalNinos").textContent = "Total Niños: " + ninos;
    document.getElementById("totalAdolescentes").textContent = "Total Adolescentes: " + adolescentes;
    document.getElementById("totalAdultos").textContent = "Total Adultos: " + adultos;
    document.getElementById("totalAncianos").textContent = "Total Ancianos: " + ancianos;
    document.getElementById("promedioEdades").textContent = "Edad Promedio: " + edadPromedio.toFixed(2);
}

// Agregar evento de clic para el botón "Generar"
document.getElementById("generar").addEventListener("click", function () {
    // Generar 100 edades aleatorias y almacenarlas en un array
    const edades = [];
    for (let i = 0; i < 100; i++) {
        edades.push(generarEdad(0, 100));
    }

    // Calcular y mostrar el conteo de edades
    calcularConteoEdades(edades);
});

// Agregar evento de clic para el botón "Limpiar"
document.getElementById("limpiar").addEventListener("click", function () {
    // Limpiar los resultados
    document.getElementById("edades").textContent = "";
    document.getElementById("conteo").textContent = "";
    document.getElementById("totalBebes").textContent = "";
    document.getElementById("totalNinos").textContent = "";
    document.getElementById("totalAdolescentes").textContent = "";
    document.getElementById("totalAdultos").textContent = "";
    document.getElementById("totalAncianos").textContent = "";
    document.getElementById("promedioEdades").textContent = "";
});