// Función para calcular el costo del boleto
function calcularCostoBoleto() {
    // Obtener los valores del formulario
    const numBoleto = parseInt(document.getElementById("numBoleto").value);
    const nombreCliente = document.getElementById("nomCliente").value;
    const destino = document.getElementById("destino").value;
    const tipoViaje = document.getElementById("tipoViaje").value;
    const precio = parseFloat(document.getElementById("precio").value);

    // Validar el tipo de viaje
    if (tipoViaje != "Sencillo" && tipoViaje != "Doble") {
        alert("Tipo de viaje no válido. Debe ser 1 para sencillo o 2 para doble.");
        return;
    }

    if (isNaN(precio)) {
        alert("El precio no es un número válido.");
        return;
    }

    // Calcular el costo base
    let costoBase = precio;
    if (tipoViaje == "Doble") {
        costoBase += precio * 0.8; // Incrementar en un 80% para viaje doble
    }

    // Calcular el impuesto
    const impuesto = costoBase * 0.16;

    // Calcular el costo total
    const costoTotal = costoBase + impuesto;

    // Mostrar el resultado en etiquetas
    document.getElementById("subtotal").textContent = "$" + costoBase.toFixed(2);
    document.getElementById("impuesto").textContent = "$" + impuesto.toFixed(2);
    document.getElementById("total").textContent = "$" + costoTotal.toFixed(2);
}

// Llamar a la función para calcular el costo del boleto cuando se haga clic en el botón "Calcular"
document.getElementById("calcular").addEventListener("click", calcularCostoBoleto);

// Función para limpiar el formulario
function limpiarFormulario() {
    document.getElementById("numBoleto").value = "";
    document.getElementById("nomCliente").value = "";
    document.getElementById("destino").value = "";
    document.getElementById("tipoViaje").value = "Sencillo";
    document.getElementById("precio").value = "";
    document.getElementById("subtotal").textContent = "";
    document.getElementById("impuesto").textContent = "";
    document.getElementById("total").textContent = "";
}

// Llamar a la función para limpiar el formulario cuando se haga clic en el botón "Limpiar"
document.getElementById("limpiar").addEventListener("click", limpiarFormulario);

// Función para enviar los datos del formulario (puedes implementar esta función según tus necesidades)
function enviarFormulario() {
    // Puedes implementar aquí el envío de datos a un servidor o el almacenamiento de datos, según tus necesidades.
    alert("Datos enviados (implementa esta función según tus necesidades).");
}

// Llamar a la función para enviar los datos del formulario cuando se haga clic en el botón "Enviar"
document.getElementById("enviar").addEventListener("click", enviarFormulario);